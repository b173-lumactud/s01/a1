@extends('layouts.app')

@section('title')
{{ $title }}
@endsection


@section('content')
    <h1>Services</h1>
    <p>This is services section</p>


@if(count($services) > 0)
    @foreach($services as $topic)
        <li>{{ $topic }}</li>
    @endforeach
    @else
        <p>Nothing to display</p>
@endif

@endsection